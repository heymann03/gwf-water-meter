/**
 * Decode function is the main entry point in the script,
 * and you should not delete it.
 *
 * Arguments:
 * 'payload' - an array of bytes, raw payload from sensor
 * 'port' - an integer value in range [0, 255], f_port value from sensor
 */
var hexPayload = '';
var result = new Object();
Object.keys(payload).forEach(function(key) {
    thishex = Number(payload[key]).toString(16);
    thishex = thishex.length < 2 ? "" + "0" + thishex : thishex;
    hexPayload += thishex;
});
//result.RawPayload = hexPayload;

/*function for reversing the string*/
function reverseString(str) {
    return str.match(/.{1,2}/g).reverse().join("");
}

// function for ISO time format
Date.prototype.toIsoString = function() {
    var tzo = -this.getTimezoneOffset(),
        dif = tzo >= 0 ? '+' : '-',
        pad = function(num) {
            var norm = Math.floor(Math.abs(num));
            return (norm < 10 ? '0' : '') + norm;
        };
    return this.getFullYear() +
        '-' + pad(this.getMonth() + 1) +
        '-' + pad(this.getDate()) +
        'T' + pad(this.getHours()) +
        ':' + pad(this.getMinutes()) +
        ':' + pad(this.getSeconds()) +
        dif + pad(tzo / 60) +
        ':' + pad(tzo % 60);
}

//function for padding a string
function paddedStr(padStr, userStr) {
    if (userStr.length < padStr.length) {
        return (padStr + userStr).slice(-padStr.length);
    } else {
        return (userStr + padStr).substring(0, padStr.length);
    }
}

// function for choosing additional functions
function additionalFunctions(binary) {
    var bit1 = (binary.charAt(1) == 0) ? 'No-Continuous Flow' : 'Continuous Flow';
    var bit3 = (binary.charAt(3) == 0) ? 'No-Broken Pipe' : 'Broken Pipe';
    var bit5 = (binary.charAt(5) == 0) ? 'No-Battery Low' : 'Battery Low';
    var bit6 = (binary.charAt(6) == 0) ? 'No-Backflow' : 'Backflow';
    var bit7 = (binary.charAt(7) == 0) ? 'No' : 'No Usage';
    return {
        'bit1': bit1,
        'bit3': bit3,
        'bit5': bit5,
        'bit6': bit6,
        'bit7': bit7
    }
}

// function for battery lifetime
function batteryLifetime(binary) {
    var batteryLifetimePadded = paddedStr('00000000', binary);
    var numberOfSemesters = binary.charAt(4) * Math.pow(2, 0) + binary.charAt(3) * Math.pow(2, 1) + binary.charAt(2) * Math.pow(2, 2) + binary.charAt(1) * Math.pow(2, 3) + binary.charAt(0) * Math.pow(2, 4);
    return numberOfSemesters;
}

// function to automate the selection of the decimal point in the meter reading value. Can be used if required
var meterReadingValueM3 = (parseInt(reverseString(hexPayload.substring(24, 32)), 16).toString(10));
//console.log(meterReadingValueM3);
function decimalPointSelection(point) {
    switch (point) {
        case '16':
            var value = meterReadingValueM3 / 1000000;
            break;
        case '17':
            var value = Math.round(meterReadingValueM3 * 100000) / 100000;
            break;
        case '18':
            var value = Math.round(meterReadingValueM3 * 10000) / 10000;
            break;
        case '19':
            var value = meterReadingValueM3 / 1000;
            break;
        case '20':
            var value = Math.round(meterReadingValueM3 * 100) / 100;
            break;
        case '21':
            var value = Math.round(meterReadingValueM3 * 10) / 10;
            break;
        case '22':
            var value = Math.round(meterReadingValueM3 * 1) / 1;
            break;
        default:
            console.log('Meter reached limit');
    }
    return value;
}

var date = new Date();

//byte reference in comments: 1 indexing
function decode(payload, port) {
    if (hexPayload.substring(0, 2) == 2 || hexPayload.substring(0, 2) == 50) { //protocol type byte[1]
        result.GWF = (parseInt(reverseString(hexPayload.substring(2, 6)), 16).toString(10)); // meter manufacturer ID byte[2-3]
        result.meterID = (parseInt(reverseString(hexPayload.substring(6, 14)), 16).toString(10)); // meterID bytes[4-7]
        result.medium = 'Water'; // meter medium byte[8]
        result.actualityDurationMin = (parseInt(reverseString(hexPayload.substring(18, 22)), 16).toString(10))
        result.volumeVIF = (hexPayload.substring(22, 24)); //code that decides number of digits after decimal
        result.meter_reading_value = decimalPointSelection(parseInt(hexPayload.substring(22, 24), 16).toString(10)); //meter reading value bytes[13-16]
        result.value_unit = 'M3';
        var additionalFunctionsBinary = parseInt(hexPayload.substring(32, 34), 16).toString(2);
        var additionalFunctionsPadded = paddedStr('00000000', additionalFunctionsBinary);
        result.additionalFunctions = additionalFunctions(additionalFunctionsPadded);
        result.batteryLifetime = batteryLifetime(parseInt(hexPayload.substring(34, 36), 16).toString(2)) + ' semesters';
        result.measurement_timestamp = date.toIsoString();
        result.port = port;
        result.RawPayload = hexPayload;
    }
    return result;
}
